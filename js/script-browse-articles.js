var checkSearchFieldMargin = function(){
    var leftMargin;
    if ($('nav').is(':visible')) {
        leftMargin = $('.top-container .logo').outerWidth() + $('nav').outerWidth();
    } else {
        leftMargin = $('.top-container .logo').outerWidth() + $('#main-menu').outerWidth();
    }
    
    if ($(window).width() < 440) {
        leftMargin = $('#main-menu').width();
    }
    
    if ($('body').css('direction') == 'rtl') {
        $('.search-wrap').css({'margin-right': leftMargin + 'px'});
    } else {
        $('.search-wrap').css({'margin-left': leftMargin + 'px'});
    }
}

var dropdownSwitch = function(){
	$('body').on('click', function(){
		$('.submenu-cont').removeClass('active');
	});
    
	$('body').on('click', '.expander', function(e){
		var $parentContainer = $(this).parents('.submenu-cont');
        $('.active').removeClass('active');
		if ($parentContainer.hasClass('active')) {
			$parentContainer.removeClass('active');
		} else {
			$parentContainer.addClass('active');
		}
		e.stopPropagation();
	});
    
    $('body').on('mouseenter', '.expander', function(){
        $('.active').removeClass('active');
    });
    
    $('body').on('click', '.search-wrap .close', function(e){
        e.preventDefault();
        var $clicked = $(this);
        $clicked.parents('.container').removeClass('search-active');
        if ($('body').css('direction') == 'rtl') {
            $('.search-active').find('.search-wrap').css('margin-right', 0);
        } else {
            $('.search-active').find('.search-wrap').css('margin-left', 0);
        }
        e.stopPropagation();
    });
    
    $('body').on('click', '.search-wrap .search', function(e){
        var $clicked = $(this);
        $('.active').removeClass('active');
        var logoWidth, navWidth;
        $clicked.parents('.container').addClass('search-active');
        
        if (($(window).width() < 840) && ($(window).width() > 320)) {
            logoWidth = 0;
            navWidth = 0;
        } else if ($(window).width() > 840) {
            logoWidth = $('.logo').outerWidth(true);
            navWidth = $('nav').outerWidth(true);
        } else {
            logoWidth = 0;
            navWidth = 0;
        }
        
        var offset = logoWidth + navWidth + $('.top-container .mobile-menu-trigger').outerWidth(true) + 18;

        if ($('body').css('direction') == 'rtl') {
            $('.search-active').find('.search-wrap').css('margin-right', offset);
        } else {
            $('.search-active').find('.search-wrap').css('margin-left', offset);
        }
        e.stopPropagation();
    });
}

var mobileMenuInit = function(){
    var sideLeft, sideRight;
    
    if ($('body').css('direction') == 'rtl') {
        sideLeft = 'right';
        sideRight = 'left';
    } else {
        sideLeft = 'left';
        sideRight = 'right';
    }
    
    if ($(window).width() < 1000) {
        $('#main-menu').sidr({
            name: 'main',
            source: 'nav',
            side: sideLeft
        });

        $('#your-menu, .userpic').sidr({
            name: 'user',
            source: '.userinfo',
            side: sideRight
        });

        $('.inbox.expander').sidr({
            name: 'inbox',
            source: '.inbox-cont',
            side: sideRight
        });

        $('#userbar .mobile-menu-trigger').sidr({
            name: 'ubar',
            source: '#bar-nav-cont',
            side: sideLeft
        });

        $('#contact, .submenu-cont').sidr({
            name: 'contact-user',
            source: '.main-stats .submenu-cont',
            side: sideRight
        });
        
        $(window).touchwipe({
            wipeRight: function() {
                  $.sidr('close', 'contact-user');
                  $.sidr('close', 'inbox');
                  $.sidr('close', 'user');
            },
            wipeLeft: function() {
                  $.sidr('close', 'main');
                  $.sidr('close', 'ubar');
            },
            preventDefaultEvents: false
        });
        
        $('body').on('click', function(e) {
            $.sidr('close', 'contact-user');
            $.sidr('close', 'inbox');
            $.sidr('close', 'user');
            $.sidr('close', 'main');
            $.sidr('close', 'ubar');
            e.stopPropagation();
        });
        
    } else {
        if ($('#main, #user, #inbox, #ubar, #contact-user').length > 0) {
            $('#main, #user, #inbox, #ubar, #contact-user').remove();
        }
    }
}

$(document).ready(function(){
    var wx3 = 378, vx2 = 350;
    var wW = $(window).width() + 16;
    var gridContainer;
    
    checkSearchFieldMargin();
    dropdownSwitch();
    mobileMenuInit();
    
    if (wW > 580) {
        gridContainer = new Masonry ('.filter-articles-results', {
            itemSelector: 'article',
            columnWidth: 'article',
            percentPosition: true,
            gutter: '.gutter-sizer',
			transitionDuration: 0
        });
    }
            
    $('.aside-collapse').on('click', function(e){
        e.preventDefault();
        wW = $(window).width() + 16;
        var $aside = $(this).closest('aside');
		var $articlesContainer = $('.filter-articles-results');
        $aside.toggleClass('folded');
//		$articlesContainer.addClass('moving');
        
        if (wW > 1000 || gridContainer == undefined) {
//            setTimeout(function(){
//				$articlesContainer.removeClass('moving');
				setTimeout(function(){
					gridContainer.layout();
				}, 350);
//            }, 300);
        }

    });
    
    $(window).resize(function(){
        checkSearchFieldMargin();
        wW = $(window).width() + 16;
        if (wW > 580 && gridContainer == undefined) {
            gridContainer = new Masonry ('.filter-articles-results', {
                itemSelector: 'article',
                columnWidth: 'article',
                percentPosition: true,
                gutter: '.gutter-sizer',
				transitionDuration: 0
            });
        } else if (wW <= 580 && gridContainer != undefined) {
            gridContainer.destroy();
        }
    });
});