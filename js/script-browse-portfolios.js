$(document).ready(function(){
    $('.aside-collapse').on('click', function(e){
        e.preventDefault();
        var $aside = $(this).closest('aside');
        $aside.toggleClass('folded');
    });
	
	$('.portfolio-list-item a').on('click', function(e){
		e.preventDefault();
		$('body').css({'overflow': 'hidden'});
		$('#show-portfolio').fadeIn('200').load('portfolio.html', function(){
            $('.modal-header').find('a').show();
        });
	});
	
	$('body').on('click', '.modal-header a', function(e){
		e.preventDefault();
		$('body').css({'overflow': 'auto'});
		$('#show-portfolio').fadeOut('200', function(){
            $(this).children('div').remove();
        });
	});
});