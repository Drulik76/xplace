var checkSearchFieldMargin = function(){
    var leftMargin;
    if ($('nav').is(':visible')) {
        leftMargin = $('.top-container .logo').outerWidth() + $('nav').outerWidth();
    } else {
        leftMargin = $('.top-container .logo').outerWidth() + $('#main-menu').outerWidth();
    }
    
    if ($(window).width() < 440) {
        leftMargin = $('#main-menu').width();
    }
    
    if ($('body').css('direction') == 'rtl') {
        $('.search-wrap').css({'margin-right': leftMargin + 'px'});
    } else {
        $('.search-wrap').css({'margin-left': leftMargin + 'px'});
    }
}

var replaceBlocks = function(){
    if ($(window).width() > 1207) {
        if($('#left-column').length == 0) {
            $('#skillset, #evaluations').prependTo('.content-cols > .container');
            $('#evaluations, #skillset').wrapAll('<div id="left-column"></div>');
        }
    } else {
        if ($('#evaluations, #skillset').parent().attr('id') == 'left-column'){
            $('#evaluations, #skillset').unwrap();
            $('#skillset').insertAfter('#about');
            $('#evaluations').insertAfter('#portfolio');
            $('#left-column').remove();
        }
    }
}

var languages = function(){
	var $newContent;
	var $oldContent;
	var $currentBlock;
    
	$('body').on('click', '.languages .edit-tools a', function(e){
		e.preventDefault();
        var $clicked = $(this);
		var action = $clicked.attr('class');
		var $parentBlock = $clicked.closest('.block');
		var blockType = $parentBlock.attr('id');
		
		switch (action) {
			case 'add':
                $parentBlock.removeClass('no-content');
                $currentBlock = $parentBlock.find('.block-content').eq(0);
                
                var languageList = '<div class="item form-item new-content"><select data-placeholder="Choose a Language" name="language"><option> </option><option value="English">English</option><option value="French">French</option><option value="Arabic">Arabic</option><option value="Spanish">Spanish</option><option value="Portuguese">Portuguese</option><option value="German">German</option><option value="Russian">Russian</option><option value="Italian">Italian</option><option value="Chinese">Chinese</option><option value="Dutch">Dutch</option><option value="Malay/Indonesian">Malay/Indonesian</option><option value="Tamil">Tamil</option><option value="Swahili">Swahili</option><option value="Serbo-Croatian">Serbo-Croatian</option><option value="Persian language">Persian language</option><option value="Bengali">Bengali</option><option value="Hindi">Hindi</option><option value="Armenian">Armenian</option><option value="Turkish">Turkish</option><option value="Romanian">Romanian</option><option value="Swedish">Swedish</option><option value="Greek">Greek</option><option value="Korean">Korean</option><option value="Albanian">Albanian</option><option value="Urdu">Urdu</option></select></div>';
                $(languageList).prependTo($currentBlock);
                beautifyInputs($parentBlock, blockType);
			break;
			case 'cancel':
                $parentBlock.removeClass('editing').find('.new-content').remove();
                if ($parentBlock.find('.item').length == 0) {
                    $parentBlock.addClass('no-content');
                }
			break;
			case 'save':
                var $newContent;
                var $oldContent = $('<div class="item"><p data-target="language"></p><div class="edit-tools"><a class="remove" href="#" role="button"></a></div></div>');

                            
                $newContent = $parentBlock.find('.new-content').eq(0);
                $oldContent.insertBefore($newContent);
                $newContent.find('input, textarea, select').each(function(){
                    var target = $(this).attr('name');
                    $oldContent.find('[data-target="'+target+'"]').html($(this).val());
                    $oldContent.find('[data-link="'+target+'"]').prop('href', $(this).val());
                });

                $newContent.remove();
                $parentBlock.removeClass('editing');
                $clicked.closest('.block-header').removeClass('editing'); 

			break;
			case 'remove':
                if ($clicked.parents('.item').length > 0) {
                    $clicked.parents('.item').remove();
                    
                if (($parentBlock).find('.item').length == 0) {
                    $parentBlock.addClass('no-content');
                }

			break;
		}
        }
    });
    $('body').on('change', 'select', function(e){
        $(this).parents('.languages').find('.save').trigger('click');
    });
}

var checkBoxDate = function($checked){
    var field = $checked.closest('.item').find('input[name="end"]');
    if ($checked.is(':checked')) {
        field.attr('disabled', true).val('now');
    } else {
        field.removeAttr('disabled');
    }
    if ($checked.closest('.item').find('input[name="end"]').val() == 'now') {
        $checked.trigger('click');
    }
}

var beautifyInputs = function(){
    $('textarea').textareaAutoSize();
    if ($('body').css('direction') == 'rtl') {
        $('.datepicker-cont').Zebra_DatePicker({format: 'M Y', direction: false, offset: [-340, 240]});
    } else {
        $('.datepicker-cont').Zebra_DatePicker({format: 'M Y', direction: false, offset: [5, 240]});
    }
    $('select').chosen();
    checkBoxDate($('input.still'));
}

var scrollTo = function(scrollTarget) {
    var positionToScroll = $(scrollTarget).offset().top - stickyBar.userBarHeight;
    $('html, body').animate({scrollTop: positionToScroll}, 500);
}

var scrollToBlock = function(clicked) {
	$('html, body').on('click', clicked, function(e){
		e.preventDefault();
        scrollTo($(this).attr('href'));
	});
}

var scrollToId = function(idScrollTo) {
	var positionToScroll = $('#' + idScrollTo).offset().top - stickyBar.userBarHeight;
	$('html, body').animate({scrollTop: positionToScroll}, 500);
}

var dropdownSwitch = function(){
	$('body').on('click', function(){
		$('.submenu-cont').removeClass('active');
	});
    
	$('body').on('click', '.expander', function(e){
		var $parentContainer = $(this).parents('.submenu-cont');
        $('.active').removeClass('active');
		if ($parentContainer.hasClass('active')) {
			$parentContainer.removeClass('active');
		} else {
			$parentContainer.addClass('active');
		}
		e.stopPropagation();
	});
    
    $('body').on('mouseenter', '.expander', function(){
        $('.active').removeClass('active');
        $('.search-active').removeClass('search-active');
    });
    
    $('body').on('click', '.search-wrap .close', function(e){
        e.preventDefault();
        var $clicked = $(this);
        $clicked.parents('.container').removeClass('search-active');
        if ($('body').css('direction') == 'rtl') {
            $('.search-active').find('.search-wrap').css('margin-right', 0);
        } else {
            $('.search-active').find('.search-wrap').css('margin-left', 0);
        }
        e.stopPropagation();
    });
    
    $('body').on('click', '.search-wrap .search', function(e){
        var $clicked = $(this);
        $('.active').removeClass('active');
        var logoWidth, navWidth;
        $clicked.parents('.container').addClass('search-active');
        
        if (($(window).width() < 840) && ($(window).width() > 320)) {
            logoWidth = 0;
            navWidth = 0;
        } else if ($(window).width() > 840) {
            logoWidth = $('.logo').outerWidth(true);
            navWidth = $('nav').outerWidth(true);
        } else {
            logoWidth = 0;
            navWidth = 0;
        }
        
        var offset = logoWidth + navWidth + $('.top-container .mobile-menu-trigger').outerWidth(true) + 18;

        if ($('body').css('direction') == 'rtl') {
            $('.search-active').find('.search-wrap').css('margin-right', offset);
        } else {
            $('.search-active').find('.search-wrap').css('margin-left', offset);
        }
        e.stopPropagation();
    });
}

var stickyBar = {
	heightToShowBar: $('.top-container').height() + $('.main-bg-img').height(),
	$barToShow: $('#userbar'),
	userBarHeight: $('#userbar').height(),
	$linkToHighlight: null,
	
	init: function(){
		$(window).on('scroll load', function(){
			stickyBar.stickToTop();
			stickyBar.activateUserbarLinks();
		});
	},
	stickToTop: function(){
		if ($(window).scrollTop() > this.heightToShowBar) {
			this.$barToShow.addClass('fixed');
		} else {
			this.$barToShow.removeClass('fixed');
		}
	},
	activateUserbarLinks: function(){
		var lowestValue = $('body').height();
		$('.bar-nav').find('a').each(function(){
			var $current = $(this);
			var link = $current.attr('href');
			var value = Math.abs($(window).scrollTop() - Math.abs($(link).offset().top) + stickyBar.userBarHeight);
            if (value < lowestValue) {
				lowestValue = value;
				stickyBar.$linkToHighlight = $current;
			}
		});
		$('.bar-nav a').removeClass('current');
		stickyBar.$linkToHighlight.addClass('current');
	}
}

var mobileMenuInit = function(){
    var sideLeft, sideRight;
    
    if ($('body').css('direction') == 'rtl') {
        sideLeft = 'right';
        sideRight = 'left';
    } else {
        sideLeft = 'left';
        sideRight = 'right';
    }
    
    if ($(window).width() < 1000) {
        $('#main-menu').sidr({
            name: 'main',
            source: 'nav',
            side: sideLeft
        });

        $('#your-menu, .userpic').sidr({
            name: 'user',
            source: '.userinfo',
            side: sideRight
        });

        $('.inbox.expander').sidr({
            name: 'inbox',
            source: '.inbox-cont',
            side: sideRight
        });

        $('#userbar .mobile-menu-trigger').sidr({
            name: 'ubar',
            source: '#bar-nav-cont',
            side: sideLeft
        });

        $('#contact, .submenu-cont').sidr({
            name: 'contact-user',
            source: '.main-stats .submenu-cont',
            side: sideRight
        });
        
        $(window).touchwipe({
            wipeRight: function() {
                  $.sidr('close', 'contact-user');
                  $.sidr('close', 'inbox');
                  $.sidr('close', 'user');
            },
            wipeLeft: function() {
                  $.sidr('close', 'main');
                  $.sidr('close', 'ubar');
            },
            preventDefaultEvents: false
        });
        
        $('body').on('click', function(e) {
            $.sidr('close', 'contact-user');
            $.sidr('close', 'inbox');
            $.sidr('close', 'user');
            $.sidr('close', 'main');
            $.sidr('close', 'ubar');
            e.stopPropagation();
        });
        
    } else {
        if ($('#main, #user, #inbox, #ubar, #contact-user').length > 0) {
            $('#main, #user, #inbox, #ubar, #contact-user').remove();
        }
    }
}


$(function(){
	
//	$('.main-bg-img').parallax();
    checkSearchFieldMargin();
    replaceBlocks();
    languages();
    beautifyInputs();
    dropdownSwitch();
    mobileMenuInit();
    stickyBar.init();
    
    $(window).resize(function(){
        mobileMenuInit();
    });
    
    $('.portfolio-item-overlay').on('click', function(e){
		e.preventDefault();
		$('body').css({'overflow': 'hidden'});
		$('#show-portfolio').fadeIn('200').load('portfolio.html', function(){
            $('.modal-header').find('a').show();
        });
	});

	$('body').on('click', '.modal-header a', function(e){
		e.preventDefault();
		$('body').css({'overflow': 'auto'});
		$('#show-portfolio').fadeOut('200', function(){
            $(this).children('div').remove();
        });
	});
    $('input[type="phone"]').mask("+999 (999) 999-99-99");
    
    $('html, body').on('focus', 'textarea', function(){
        $(this).textareaAutoSize();
    });
    
    $('#complete_profile').on('click', function(){
        $('#edit_phone').show();
    });
    
    $.ajaxSetup ({
        cache: false,
        dataType: "html",
        type: ("GET")
    });
    
    scrollToBlock('.bar-nav a');
    
    $('select').chosen();
    
    $(window).on('resize', function(){
        replaceBlocks();
        checkSearchFieldMargin();
    });
    
    $('input.still').each(function(){
        checkBoxDate($(this));
    });
    
    $('body').on('change', 'input.still', function(){
        checkBoxDate($(this));
    });
    
    var parallaxed = $('.main-bg-img__bg-overlay').find('img');
    var oldScrollPos = $(window).scrollTop(),
        permanentTopPosition = parseInt(parallaxed.css('top'));

    $(window).scroll(function(){
        var scrollPos = $(window).scrollTop();
        var direction;
        var difference;
        if(scrollPos > oldScrollPos) {
            direction = 'down';
            difference = scrollPos - oldScrollPos;
        } else {
            direction = 'up';
            difference = oldScrollPos - scrollPos;
        }



        console.log(scrollPos);
        if (scrollPos < $('.top-container').height() + $('.main-bg-img').height()){
            var newTop = parseInt(parallaxed.css('top'));
            if (direction == 'down') {
                newTop -= difference/3;
            } else {
                newTop += difference/3;
            }
            if(newTop > permanentTopPosition) {
                newTop = permanentTopPosition;
            }
            console.log(direction, newTop);
            parallaxed.css('top', newTop + 'px');
        }
        oldScrollPos = scrollPos;
    });
});